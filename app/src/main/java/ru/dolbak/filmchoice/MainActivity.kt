package ru.dolbak.filmchoice

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {
    private lateinit var films: ArrayList<String>
    private lateinit var textView: TextView
    private lateinit var newFilmButton: Button
    private lateinit var resetButton: Button
    private lateinit var rand: Random

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        films = resources.getStringArray(R.array.films).toCollection(ArrayList())
        textView = findViewById(R.id.textView)
        newFilmButton = findViewById(R.id.newFilmButton)
        resetButton = findViewById(R.id.resetButton)
        rand = Random()
    }

    fun onClick(view: View) {
        if (view.id == R.id.newFilmButton){
            if (films.size <= 0){
                textView.text = "Фильмы закончились"
            }
            else{
                val index = rand.nextInt(films.size)
                textView.text = films[index]
                films.removeAt(index)
            }
        }
        else if (view.id == R.id.resetButton){
            textView.text = "Нажмите кнопку"
            films = resources.getStringArray(R.array.films).toCollection(ArrayList())
        }
    }
}